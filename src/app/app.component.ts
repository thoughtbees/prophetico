import { Component } from '@angular/core';
import { FilterPipe } from './pipe';
import { DataPipe } from './data';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Prophetico';

  constructor(private DataPipe: DataPipe,private FilterPipe: FilterPipe) {}
 
  // Data Instilization
  items: any[] = [{name:"Artificial intelligence",value:10},{name:"virtual reality",value:20},{name:"Internet of things",value:30},{name:"Security",value:40},{name:"Cloud computing",value:50},{name:"Fintech",value:60},{name:"Big data analytics",value:70},{name:"Smart homes",value:80},{name:"Health care",value:90},{name:"augmented reality",value:100},{name:"Wearables",value:110},{name:"Fast moving consumer goods",value:120}];

  
   // Doughnut Variables
  public doughnutChartLabels:string[] = [ ];
  public doughnutChartData:number[] = [ ];
  public doughnutChartType:string = 'pie';
  public chartColors: any[] = [
      { 
        backgroundColor:["#FF7360", "#6FC8CE", "#FAFFF2", "#FFFCC4", "#FF7360", "#6FC8CE", "#FAFFF2", "#FFFCC4","#FF7360", "#6FC8CE", "#FAFFF2", "#FFFCC4"] 
      }]; 
  
  // events
  public chartClicked(e:any):void {
    console.log(e);
  }
 
  public chartHovered(e:any):void {
    console.log(e);
  }

  // Function For Updating Chart Data
  donut(data,queryString) { 
   
   let FilteredData = this.FilterPipe.transform(data,queryString);
   let Data = this.DataPipe.transform(FilteredData,'a');

   let Labels = this.DataPipe.transform(FilteredData,'b');

   //this.doughnutChartLabels = Labels;
   this.doughnutChartData = Data;
   this.doughnutChartLabels.length = 0;
for (let i = Labels.length - 1; i >= 0; i--) {
  this.doughnutChartLabels.push(Labels[i]);
}
   console.log(Data);
  }

}
