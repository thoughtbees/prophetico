import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'DataPipe',
})
export class DataPipe implements PipeTransform {
    transform(value: any,args: string): any {
    if (!value) return value;

 let keys = [];
 let lables = [];	

 for (let key in value) {
        keys.push(value[key].value);
        lables.push(value[key].name);
    
    }

   if (args == "a") {
        return keys;
   }else{
        return lables;
  }
   }
}