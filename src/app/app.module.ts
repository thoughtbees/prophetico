import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { FilterPipe } from './pipe';
import { DataPipe } from './data';


@NgModule({
  declarations: [
    AppComponent,
    FilterPipe,
    DataPipe
  ],
  imports: [
    BrowserModule,
    Ng2SearchPipeModule,
    FormsModule,
    ChartsModule
  ],
  providers: [DataPipe,FilterPipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
